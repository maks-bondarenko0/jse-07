package ru.t1.bondarenko.tm;

import ru.t1.bondarenko.tm.api.component.IBootstrap;
import ru.t1.bondarenko.tm.component.Bootstrap;

public final class Application {

    public static void main(final String[] args) {
        final IBootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}