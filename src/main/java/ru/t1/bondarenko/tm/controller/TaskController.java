package ru.t1.bondarenko.tm.controller;

import ru.t1.bondarenko.tm.api.controller.ITaskController;
import ru.t1.bondarenko.tm.api.service.ITaskService;
import ru.t1.bondarenko.tm.model.Project;
import ru.t1.bondarenko.tm.model.Task;
import ru.t1.bondarenko.tm.util.TerminalUtil;

import java.util.List;

public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        System.out.println("[SHOW TASKS]");
        final List<Task> projects = taskService.findAll();
        int index = 1;
        for (final Task task: projects) {
            final String name = task.getName();
            final String description = task.getDescription();
            System.out.printf("%s. %s : %s \n", index, name, description);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) System.out.println("[ERROR]");
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[TASK CLEAR]");
        taskService.deleteAll();
        System.out.println("[OK]");
    }

}
