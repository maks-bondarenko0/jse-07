package ru.t1.bondarenko.tm.api.service;

import ru.t1.bondarenko.tm.api.repository.IProjectRepository;
import ru.t1.bondarenko.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name);

    Project create(String name, String description);

}
