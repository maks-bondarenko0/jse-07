package ru.t1.bondarenko.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void createTask();

    void clearTasks();

}
