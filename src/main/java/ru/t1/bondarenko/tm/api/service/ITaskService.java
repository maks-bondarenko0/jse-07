package ru.t1.bondarenko.tm.api.service;

import ru.t1.bondarenko.tm.api.repository.ITaskRepository;
import ru.t1.bondarenko.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name);

    Task create(String name, String description);

}
