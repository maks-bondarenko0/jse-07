package ru.t1.bondarenko.tm.api.repository;

import ru.t1.bondarenko.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    Task add(Task task);

    void deleteAll();

}
