package ru.t1.bondarenko.tm.repository;

import ru.t1.bondarenko.tm.api.repository.ITaskRepository;
import ru.t1.bondarenko.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    public TaskRepository() {
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public Task add(Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public void deleteAll() {
        tasks.clear();
    }

}
